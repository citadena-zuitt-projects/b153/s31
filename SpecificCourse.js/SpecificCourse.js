import {useParams} from "react-router-dom"
import {Card, Button} from "react-bootstrap"
import {useState, useEffect } from "react";

export default function SpecificCourse() {
  const {courseId} = useParams();
  const [courseData, setCourseData] = useState({});
  
  const {_id, name, description, price, isActive} = courseData  
  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
      .then((res) => res.json())
      .then(data =>setCourseData(data))
  };

  useEffect(() => fetchData(), []);

  return <>
    <Card className="my-3">
        <Card.Header className="bg-dark text-white text-center pb-0">
            <h4>{name}</h4>
        </Card.Header>
        <Card.Body>
            <Card.Text>{description}</Card.Text>
            <h6>Price: {price}</h6>
        </Card.Body>        
        <Card.Footer className="d-grid gap-2">
            <Button variant="primary">Enroll</Button>
        </Card.Footer>
    </Card>
  </>;
}
