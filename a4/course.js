let params = new URLSearchParams(window.location.search)

let courseId = params.get("courseId")
let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDescription");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");

fetch(`http://localhost:4000/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	// console.log(data)
    courseName.innerHTML = data.name;
    courseDescription.innerHTML = data.description;
    coursePrice.innerHTML = data.price;

    if(!token){
    	enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-secondary" disabled>Please log in to enroll</button>`
    }else{
    	enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`
    }
})