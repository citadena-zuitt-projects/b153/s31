let params = new URLSearchParams(window.location.search)

let courseId = params.get("courseId")

fetch(`http://localhost:4000/courses/${courseId}`)
.then(res => res.json())
.then(data => {	        
    
    data.isActive = false;

    fetch(`http://localhost:4000/courses/${courseId}`,{
        method: "DELETE",
        headers:{
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`
        },
        body: JSON.stringify({
            isActive: data.isActive
        })
    })
    .then(res => res.json())
    .then(data => {
        if(data){
            alert("Course Info successfully deleted.")
            window.location.replace("./courses.html")
        }else{
            alert("Something went wrong.")
        }
    })
})
