import {useState} from "react"

import { Card, Button } from "react-bootstrap";

export default function CourseCard({courseProps}){
  const {id, name, description, price, onOffer} = courseProps
  const [count, setCount] = useState(0)
  const [seats, setSeats] = useState(30)

  const enroll = () => {
    if(seats <= 0){
      alert(`No more seats for course "${name}"`)
      return false
    }
    setCount(count + 1)
    setSeats(seats - 1)
  }

  
    return(
        <Card key={id} className="my-3 cardHighlight p-3 h-100" data-aos='flip-left'>
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Text>
              <strong>Description:</strong> {description}
            </Card.Text>
            <Card.Text>
              <strong>Price:</strong> {price.toLocaleString()}             
            </Card.Text>
            <Card.Text>
              <strong>Enrollees: </strong> {count}             
            </Card.Text>
            <Button 
            variant="primary"
            onClick={enroll}
            >Enroll</Button>
          </Card.Body>
        </Card>
    )
}
