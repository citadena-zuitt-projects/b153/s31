let courseData = [
    {
        id: "wdc001",
        name: "PHP-Laravel",
        description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Similique amet maiores tempore deserunt blanditiis neque, ad, dolorem fuga modi minus impedit a rerum, esse error? Architecto cupiditate vel error? Similique.",
        price: 25000,
        onOffer: true

    },
    {
        id: "wdc002",
        name: "ExpressJS",
        description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Similique amet maiores tempore deserunt blanditiis neque, ad, dolorem fuga modi minus impedit a rerum, esse error? Architecto cupiditate vel error? Similique.",
        price: 25000,
        onOffer: true

    },
    {
        id: "wdc003",
        name: "Java-Springboot",
        description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Similique amet maiores tempore deserunt blanditiis neque, ad, dolorem fuga modi minus impedit a rerum, esse error? Architecto cupiditate vel error? Similique.",
        price: 45000,
        onOffer: true

    },
    {
        id: "wdc004",
        name: "Node.js-Express",
        description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Similique amet maiores tempore deserunt blanditiis neque, ad, dolorem fuga modi minus impedit a rerum, esse error? Architecto cupiditate vel error? Similique.",
        price: 25000,
        onOffer: true

    }
];

export default courseData;
